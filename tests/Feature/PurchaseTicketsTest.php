<?php

namespace Tests\Feature;

use App\Billing\PaymentGateway;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use App\Concert;
use App\Billing\FakePaymentGateway;

class PurchaseTicketsTest extends TestCase
{
	use DatabaseMigrations;

	public $paymentGateway;

	protected function setUp(): void
	{
		parent::setUp();

		$this->paymentGateway = new FakePaymentGateway;
		$this->app->instance(PaymentGateway::class, $this->paymentGateway);
	}

	private function orderTickets($concert, $data)
	{
		return $this->json('POST', "/concerts/{$concert->id}/orders", $data);
	}

	private function assertValidationError($response, $field)
	{
		$response->assertStatus(422);

		$issetFieldError = false;
		$decodeResponse = $response->decodeResponseJson();

		if (array_key_exists('errors', $decodeResponse)) {
			if (array_key_exists($field, $decodeResponse['errors'])) {
				$issetFieldError = true;
			}
		}

		$this->assertTrue($issetFieldError);
	}

	/** @test */
	function customer_can_purchase_tickets_to_a_published_concert()
	{
		// Arrange
		// Create a concert
		$ticketsCount = 7;

		$concertData = ['ticket_price' => 3250];
		$concert = factory(Concert::class)
			->states('published')
			->create($concertData)
			->addTickets($ticketsCount);

		// Act
		// Purchase concert tickets
		$jsonData = [
			'email'           => 'test@gmail.com',
			'ticket_quantity' => $ticketsCount,
			'payment_token'   => $this->paymentGateway->getValidTestToken(),
		];
		$response = $this->orderTickets($concert, $jsonData);

		$response->assertStatus(201);

		$expectedPrice = $concertData['ticket_price'] * $jsonData['ticket_quantity'];

		$response->assertJson([
			'email'           => $jsonData['email'],
			'ticket_quantity' => $ticketsCount,
			'amount'          => $expectedPrice
		]);
		$this->assertEquals($expectedPrice, $this->paymentGateway->totalCharges());
		$this->assertTrue($concert->hasOrderFor($jsonData['email']));

		$ticketsCount = $concert->ordersFor($jsonData['email'])->first()->ticketQuantity();
		$this->assertEquals($jsonData['ticket_quantity'], $ticketsCount);
	}

	/** @test */
	function cannot_purchase_tickets_to_an_unpublished_concert()
	{
		$concertData = ['ticket_price' => 3250];
		$concert = factory(Concert::class)
			->states('unpublished')
			->create($concertData)
			->addTickets(7);

		$jsonData = [
			'email'           => 'test@gmail.com',
			'ticket_quantity' => 7,
			'payment_token'   => $this->paymentGateway->getValidTestToken(),
		];
		$response = $this->orderTickets($concert, $jsonData);
		$response->assertStatus(404);

		$this->assertFalse($concert->hasOrderFor($jsonData['email']));
		$this->assertEquals(0, $this->paymentGateway->totalCharges());
	}

	/** @test */
	function an_order_is_not_created_if_payment_fails()
	{
		$concertData = ['ticket_price' => 3250];
		$concert = factory(Concert::class)
			->states('published')
			->create($concertData)
			->addTickets(7);

		$jsonData = [
			'email'           => 'test@gmail.com',
			'ticket_quantity' => 7,
			'payment_token'   => 'invalid-payment-token',
		];
		$response = $this->orderTickets($concert, $jsonData);

		$response->assertStatus(422);
		$this->assertFalse($concert->hasOrderFor($jsonData['email']));
	}

	/** @test */
	function cannot_purchase_more_tickets_than_remain()
	{
		$ticketsAvailable = 50;

		$concert = factory(Concert::class)
			->states('published')
			->create()
			->addTickets($ticketsAvailable);

		$jsonData = [
			'email'           => 'test@gmail.com',
			'ticket_quantity' => 51,
			'payment_token'   => $this->paymentGateway->getValidTestToken(),
		];
		$response = $this->orderTickets($concert, $jsonData);

		$response->assertStatus(422);
		$this->assertFalse($concert->hasOrderFor($jsonData['email']));
		$this->assertEquals(0, $this->paymentGateway->totalCharges());
		$this->assertEquals(50, $concert->ticketsRemaining());
	}

	/** @test */
	function email_is_required_to_purchase_tickets()
	{
		$concert = factory(Concert::class)->states('published')->create();
		$concert->addTickets(7);

		$jsonData = [
			'ticket_quantity' => 7,
			'payment_token'   => $this->paymentGateway->getValidTestToken(),
		];
		$response = $this->orderTickets($concert, $jsonData);

		$this->assertValidationError($response, 'email');
	}

	/** @test */
	function email_must_be_valid_to_purchase_tickets()
	{
		$concert = factory(Concert::class)->states('published')->create();
		$concert->addTickets(7);

		$jsonData = [
			'email'           => 'my-email-address',
			'ticket_quantity' => 7,
			'payment_token'   => $this->paymentGateway->getValidTestToken(),
		];
		$response = $this->orderTickets($concert, $jsonData);

		$this->assertValidationError($response, 'email');
	}

	/** @test */
	function ticket_quantity_is_required_to_purchase_tickets()
	{
		$concert = factory(Concert::class)->states('published')->create();
		$concert->addTickets(7);

		$jsonData = [
			'email'         => 'test@gmail.com',
			'payment_token' => $this->paymentGateway->getValidTestToken(),
		];
		$response = $this->orderTickets($concert, $jsonData);

		$this->assertValidationError($response, 'ticket_quantity');
	}

	/** @test */
	function ticket_quantity_must_be_at_least_1_to_purchase_tickets()
	{
		$concert = factory(Concert::class)->states('published')->create();
		$concert->addTickets(7);

		$jsonData = [
			'email'           => 'test@gmail.com',
			'ticket_quantity' => 0,
			'payment_token'   => $this->paymentGateway->getValidTestToken(),
		];
		$response = $this->orderTickets($concert, $jsonData);

		$this->assertValidationError($response, 'ticket_quantity');
	}

	/** @test */
	function payment_token_is_required()
	{
		$concert = factory(Concert::class)->states('published')->create();
		$concert->addTickets(7);

		$jsonData = [
			'email'           => 'test@gmail.com',
			'ticket_quantity' => 0,
		];
		$response = $this->orderTickets($concert, $jsonData);

		$this->assertValidationError($response, 'payment_token');
	}
}
