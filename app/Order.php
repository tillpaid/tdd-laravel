<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $guarded = [];

	public function concert()
	{
		return $this->belongsTo(Concert::class);
	}

	public function tickets()
	{
		return $this->hasMany(Ticket::class);
	}

	public function ticketQuantity()
	{
		return $this->tickets()->count();
	}

	public function cancel()
	{
		foreach ($this->tickets as $ticket) {
			$ticket->release();
		}

		$this->delete();
	}

	public function toArray()
	{
		$ticketQuantity = $this->ticketQuantity();

		return [
			'email'           => $this->email,
			'ticket_quantity' => $ticketQuantity,
			'amount'          => $ticketQuantity * $this->concert->ticket_price,
		];
	}
}
