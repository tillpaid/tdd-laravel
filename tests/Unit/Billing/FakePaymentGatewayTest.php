<?php

namespace Tests\Unit;

use App\Billing\FakePaymentGateway;
use App\Billing\PaymentFailedException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class FakePaymentGatewayTest extends TestCase
{
	/** @test */
	function charges_with_a_valid_payment_token_are_successful()
	{
		$paymentGateway = new FakePaymentGateway;

		$price = 2500;
		$paymentGateway->charge($price, $paymentGateway->getValidTestToken());

		$this->assertEquals($price, $paymentGateway->totalCharges());
	}

	/** @test */
	function charges_with_an_invalid_payment_token_fail()
	{
		$exceptionIsset = false;

		try {
			$paymentGateway = new FakePaymentGateway;

			$price = 2500;
			$paymentGateway->charge($price, 'not valid token');
		} catch (PaymentFailedException $e) {
			$exceptionIsset = true;
		}

		$this->assertTrue($exceptionIsset);
	}
}
