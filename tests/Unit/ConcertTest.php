<?php

namespace Tests\Unit;

use App\Concert;
use Illuminate\Foundation\Testing\RefreshDatabase;
//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Carbon\Carbon;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Exceptions\NotEnoughTicketsException;

class ConcertTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */
	public function can_get_formatted_date()
	{
		// Create a concert with a known date
		$concertData = [
			'date' => Carbon::parse('December 13, 2020 20:00'),
		];

		$concert = factory(Concert::class)->make($concertData);

		// Verify the date is formatted as expected
		$this->assertEquals('December 13, 2020', $concert->formatted_date);
	}

	/** @test */
	function can_get_formatted_start_time()
	{
		// Create a concert with a known datetime
		$concertData = [
			'date' => Carbon::parse('December 13, 2020 21:00'),
		];

		$concert = factory(Concert::class)->make($concertData);

		// Verify the datetime is formatted as expected
		$this->assertEquals('21:00', $concert->formatted_start_time);
	}

	/** @test */
	function can_get_formatted_ticket_price()
	{
		// Create a concert with a known ticket price
		$concertData = [
			'ticket_price' => 8034,
		];

		$concert = factory(Concert::class)->make($concertData);

		// Verify the ticket price if formatted as expected
		$this->assertEquals('80.34', $concert->formatted_ticket_price);
	}

	/** @test */
	function concert_with_a_published_at_date_are_published()
	{
		$publishedConcertA = factory(Concert::class)->states('published')->create();
		$publishedConcertB = factory(Concert::class)->states('published')->create();
		$unpublishedConcert = factory(Concert::class)->states('unpublished')->create();

		$publishedConcerts = Concert::published()->get();

		$this->assertTrue($publishedConcerts->contains($publishedConcertA));
		$this->assertTrue($publishedConcerts->contains($publishedConcertB));
		$this->assertFalse($publishedConcerts->contains($unpublishedConcert));
	}

	/** @test */
	function can_order_concert_tickets()
	{
		$userEmail = 'test@gmail.com';
		$ticketsQuantity = 13;

		$concert = factory(Concert::class)->create()->addTickets($ticketsQuantity);

		$order = $concert->orderTickets($userEmail, $ticketsQuantity);

		$this->assertEquals($userEmail, $order->email);
		$this->assertEquals($ticketsQuantity, $order->ticketQuantity());
	}

	/** @test */
	function cat_add_tickets()
	{
		$ticketsCount = 50;

		$concert = factory(Concert::class)->create()->addTickets($ticketsCount);

		$this->assertEquals($ticketsCount, $concert->ticketsRemaining());
	}

	/** @test */
	function tickets_remaining_does_not_include_tickets_associated_with_an_order()
	{
		$ticketsCount = 50;
		$ticketsPurchase = 20;

		$concert = factory(Concert::class)->create()->addTickets($ticketsCount);

		$concert->orderTickets('test@gmail.com', $ticketsPurchase);

		$this->assertEquals($ticketsCount - $ticketsPurchase, $concert->ticketsRemaining());
	}

	/** @test */
	function trying_to_purchase_more_tickets_than_remain_throws_an_exception()
	{
		$ticketsCount = 10;
		$ticketsPurchase = 11;
		$email = 'test@gmail.com';

		$concert = factory(Concert::class)->create()->addTickets($ticketsCount);

		try {
			$concert->orderTickets($email, $ticketsPurchase);
		} catch (NotEnoughTicketsException $e) {
			$this->assertFalse($concert->hasOrderFor($email));
			$this->assertEquals($ticketsCount, $concert->ticketsRemaining());

			return;
		}

		$this->fail("Order succeeded even though there were not enough tickets remaining.");
	}

	/** @test */
	function cannot_order_tickets_that_have_already_been_purchased()
	{
		$ticketsCount = 10;
		$concert = factory(Concert::class)->create()->addTickets($ticketsCount);

		$concert->orderTickets('test@gmail.com', 8);

		try {
			$concert->orderTickets('test1@gmail.com', 3);
		} catch (NotEnoughTicketsException $e) {
			$this->assertFalse($concert->hasOrderFor('test1@gmail.com'));
			$this->assertEquals(2, $concert->ticketsRemaining());

			return;
		}
	}
}
