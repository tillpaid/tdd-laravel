<?php

namespace Tests\Unit;

use App\Concert;
use App\Order;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class OrderTest extends TestCase
{
	use DatabaseMigrations;

	/** @test */
	function converting_to_an_array()
	{
		$data = [
			'email'           => 'test@gmail.com',
			'ticket_price'    => 1200,
			'ticket_quantity' => 10
		];

		$concert = factory(Concert::class)
			->create(['ticket_price' => $data['ticket_price']])
			->addTickets(10);
		$order = $concert->orderTickets($data['email'], $data['ticket_quantity']);
		$result = $order->toArray();

		$expected = [
			'email'           => $data['email'],
			'ticket_quantity' => $data['ticket_quantity'],
			'amount'          => $data['ticket_quantity'] * $data['ticket_price']
		];

		$this->assertEquals($expected, $result);
	}

	/** @test */
	function tickets_are_released_when_an_order_is_cancelled()
	{
		$concert = factory(Concert::class)->create()->addTickets(10);

		$order = $concert->orderTickets('test@gmail.com', 5);

		$this->assertEquals(5, $concert->ticketsRemaining());

		$order->cancel();

		$this->assertEquals(10, $concert->ticketsRemaining());
		$this->assertNull(Order::find($order->id));
	}
}
