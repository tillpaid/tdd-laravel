<?php

namespace Tests\Feature;

use App\Concert;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Carbon\Carbon;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ViewConcertListingTest extends TestCase
{
	use DatabaseMigrations;

	/**
	 * @test
	 */
	function user_can_view_a_published_concert_listing()
	{
		// Arrange
		// Create a concert
		$concertData = [
			'title'                  => 'The Red Chord',
			'subtitle'               => 'with Animosity and Lethargy',
			'date'                   => Carbon::parse('December 13, 2020 20:00'),
			'ticket_price'           => 3250,
			'venue'                  => 'The Mosh Pit',
			'venue_address'          => '123 Example Lane',
			'city'                   => 'Laraville',
			'state'                  => 'ON',
			'zip'                    => '17916',
			'additional_information' => 'For tickets, call (555) 555-5555.',
		];

		$concert = factory(Concert::class)->states('published')->create($concertData);

		// Act
		// View the concert listing
		$visitUrl = "/concerts/{$concert->id}";

		$response = $this->get($visitUrl);

		// Assert
		// See the concert details
		$response->assertSee('The Red Chord');
		$response->assertSee('with Animosity and Lethargy');
		$response->assertSee('December 13, 2020');
		$response->assertSee('20:00');
		$response->assertSee('32.50');
		$response->assertSee('The Mosh Pit');
		$response->assertSee('123 Example Lane');
		$response->assertSee('Laraville, ON 17916');
		$response->assertSee('For tickets, call (555) 555-5555.');
	}

	/** @test */
	function user_cannot_view_unpublished_concert_listings()
	{
		// Create
		$concert = factory(Concert::class)->states('unpublished')->create();

		// View data in page
		$visitUrl = "/concerts/{$concert->id}";

		$response = $this->get($visitUrl);

		$response->assertStatus(404);
	}
}
