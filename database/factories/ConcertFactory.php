<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Concert;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Concert::class, function (Faker $faker) {
    return [
		'title'                  => 'The Red Chord',
		'subtitle'               => 'with Animosity and Lethargy',
		'date'                   => Carbon::parse('+2 weeks'),
		'ticket_price'           => 3250,
		'venue'                  => 'The Mosh Pit',
		'venue_address'          => '123 Example Lane',
		'city'                   => 'Laraville',
		'state'                  => 'ON',
		'zip'                    => '17916',
		'additional_information' => 'For tickets, call (555) 555-5555.',
		'published_at'           => null,
    ];
});

$factory->state(Concert::class, 'published', function (Faker $faker) {
	return [
		'published_at' => Carbon::create('-1 week'),
	];
});

$factory->state(Concert::class, 'unpublished', function (Faker $faker) {
	return [
		'published_at' => null,
	];
});
