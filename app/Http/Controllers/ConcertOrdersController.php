<?php

namespace App\Http\Controllers;

use App\Billing\PaymentFailedException;
use App\Exceptions\NotEnoughTicketsException;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Billing\PaymentGateway;
use App\Concert;

class ConcertOrdersController extends Controller
{
	private $paymentGateway;

	public function __construct(PaymentGateway $paymentGateway)
	{
		$this->paymentGateway = $paymentGateway;
	}

	public function store($concertId)
	{
		$concert = Concert::published()->findOrFail($concertId);

		$this->validate(\request(), [
			'email'           => ['required', 'email'],
			'ticket_quantity' => ['required', 'integer', 'min:1'],
			'payment_token'   => ['required'],
		]);

		try {
			$email = \request('email');
			$ticketsQuantity = request('ticket_quantity');
			$amount = $ticketsQuantity * $concert->ticket_price;
			$token = \request('payment_token');

			// Creating the order
			$order = $concert->orderTickets($email, $ticketsQuantity);

			// Charging the customer
			$this->paymentGateway->charge($amount, $token);

			return response()->json($order, 201);
		} catch (PaymentFailedException $e) {
			$order->cancel();

			return response()->json([], 422);
		} catch (NotEnoughTicketsException $e) {
			return response()->json([], 422);
		}
	}
}
